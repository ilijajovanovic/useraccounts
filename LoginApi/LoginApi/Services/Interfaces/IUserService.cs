﻿using LoginApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginApi.Services.Interfaces
{
    public interface IUserService
    {
        UserModel Authenticate(string username, string password);
        IEnumerable<UserModel> GetAll();
        UserModel GetById(long id);
        UserModel Create(UserModel user, string password);
        void Update(UserModel userParam, string password = null);
        void Delete(long id);
    }
}
