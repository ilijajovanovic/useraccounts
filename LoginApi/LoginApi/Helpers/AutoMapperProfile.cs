﻿using AutoMapper;
using LoginApi.Dtos;
using LoginApi.Models;

namespace LoginApi.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<UserModel, UserDto>();
            CreateMap<UserDto, UserModel>();
        }
    }
}
