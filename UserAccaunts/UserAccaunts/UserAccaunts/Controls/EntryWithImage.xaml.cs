﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UserAccaunts.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EntryWithImage : ContentView
    {
        //public static BindableProperty EntryProperty = BindableProperty.Create(nameof(Entry), typeof(Entry), typeof(EntryWithImage), defaultBindingMode: BindingMode.OneWayToSource, propertyChanged: (bindable, oldVal, newVal) =>
        //{

        //});
        public Entry Entry { get; set; }

        public EntryWithImage()
        {
            InitializeComponent();

            Entry = entry;
        }
    }
}