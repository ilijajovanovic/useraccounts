﻿using System.ComponentModel;
using UserAccaunts.Models;

namespace UserAccaunts.Dto
{
    public class UserDto : INotifyPropertyChanged
    {
        public long Id { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Adress { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }

        public UserDto(UserModel model)
        {
            Id = model.Id;
            Username = model.Username;
            FirstName = model.FirstName;
            LastName = model.LastName;
            Email = model.Email;
            Adress = model.Adress;
            PhoneNumber = model.PhoneNumber;
            Password = model.Password;
            Token = model.Token;
        }

        public UserDto()
        { }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
