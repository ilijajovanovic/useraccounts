﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace UserAccaunts.Dto
{
    public class LoginDto : INotifyPropertyChanged
    {
        public string GrantType { get; set; }
        public string RefreshToken { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        [Required(ErrorMessage = "Fild is required")]
        public string Username { get; set; } 
        [Required(ErrorMessage = "Fild is required")]
        public string Password { get; set; }
        public string Scope { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
