﻿using Acr.UserDialogs;
using Polly;
using Prism.Navigation;
using Refit;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UserAccaunts.Services.Remote.Interfaces;

namespace UserAccaunts.ViewModels.Base
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected readonly IAppService AppService;

        public BaseViewModel(IAppService appService)
        {
            AppService = appService;
        }

        protected async Task InvokeServiceAsync(Func<Task> service, bool showLoading = true)
        {
            await InvokeServiceAsync(service, null, showLoading, null);
        }

        private async Task InvokeServiceAsync(Func<Task> service, Func<CancellationToken, Task> serviceCancellable, bool showLoading = true, CancellationToken? ct = null)
        {
            try
            {
                PrepareServiceCall(showLoading);
                await service();
            }
            catch (TaskCanceledException)
            {
            }
            catch (ApiException apiException)
            {
                AppService.UserDialogs.Toast(apiException.Content);
            }
            catch (ExecutionRejectedException e)
            {
                AppService.UserDialogs.Toast(e.Message);
            }
            catch (Exception e)
            {
                AppService.UserDialogs.Toast(e.Message);
            }
            finally
            {
                FinishServiceCall();
            }
        }

        public void PrepareServiceCall(bool showLoading = true)
        {
            if (showLoading)
            {
                AppService.UserDialogs.ShowLoading();
            }
        }

        public void FinishServiceCall()
        {
            AppService.UserDialogs.HideLoading();
        }
    }
}
