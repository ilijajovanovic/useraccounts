﻿using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using UserAccaunts.Helpers;
using UserAccaunts.Services.Remote.Interfaces;
using UserAccaunts.ViewModels.Base;

namespace UserAccaunts.ViewModels
{
    public class HomePageViewModel : BaseViewModel
    {
        public HomePageViewModel(IAppService appService)
            : base(appService)
        {

        }

        private DelegateCommand<object> _navigateToUserCreatePageCommand;
        public DelegateCommand<object> NavigateToUserCreatePageCommand => _navigateToUserCreatePageCommand ?? (_navigateToUserCreatePageCommand = new DelegateCommand<object>(NavigateToUserCreatePage));
        private async void NavigateToUserCreatePage(object obj)
        {
            await AppService.NavigationService.NavigateAsync(PageNames.UsersAdministrationPage);
        }
    }
}
