﻿using Acr.UserDialogs;
using Microsoft.Extensions.DependencyInjection;
using Prism.Commands;
using System;
using System.Windows.Input;
using UserAccaunts.Dto;
using UserAccaunts.Helpers;
using UserAccaunts.Services.Remote.Interfaces;
using UserAccaunts.ViewModels.Base;
using UserAccaunts.Views;
using Xamarin.Forms;
using Xamarin.Essentials;

namespace UserAccaunts.ViewModels
{
    public class LoginPageViewModel : BaseViewModel
    {
        private readonly IUserService _userService; 
        
        public UserDto User { get; set; } = new UserDto();
        public LoginDto LoginUser { get; set; } = new LoginDto();

        SensorSpeed speed = SensorSpeed.Game;
        public ConstraintExpression constraintX { get; set; } = new ConstraintExpression { Type = ConstraintType.RelativeToParent, Property = "Width", Constant = 5 };
        public ConstraintExpression constraintY { get; set; } = new ConstraintExpression { Type = ConstraintType.RelativeToParent, Property = "Height", Constant = 5 };
        public double XCoord { get; set; }
        public double YCoord { get; set; }


        public LoginPageViewModel(IAppService appService,IUserService userService)
            :base(appService)
        {
            _userService = userService;
            Gyroscope.ReadingChanged += Gyroscope_ReadingChanged;
            Gyroscope.Start(speed);
        }

        private void Gyroscope_ReadingChanged(object sender, GyroscopeChangedEventArgs e)
        {
            if(e.Reading.AngularVelocity.Y > 0.1)
            {
                if (XCoord < 20)
                {
                    XCoord += 1;
                }
                return;
            }
            if (e.Reading.AngularVelocity.Y < -0.1)
            {
                if (XCoord > -20)
                {
                    XCoord -= 1;
                }
                return;
            }
            if (e.Reading.AngularVelocity.X > 0.1)
            {
                if (YCoord < 20)
                {
                    YCoord += 1;
                }
                return;
            }
            if (e.Reading.AngularVelocity.X < -0.1)
            {
                if (YCoord > -20)
                {
                    YCoord -= 1;
                }
                return;
            }

            var data = e.Reading;
            
        }

        private DelegateCommand<object> _loginCommand;
        public DelegateCommand<object> LoginCommand => _loginCommand ?? (_loginCommand = new DelegateCommand<object>(Login));

        private async void Login(object obj)
        {
            await InvokeServiceAsync(async () => 
            {
                var response = await _userService.Authenticate(User);               

                Settings.Token = response.Token;
                await AppService.NavigationService.NavigateAsync(PageNames.HomePage);

                User.Username = string.Empty;
                User.Password = string.Empty;
            });
        }

        private DelegateCommand<object> _navigateToUserCreatePageCommand;
        public DelegateCommand<object> NavigateToUserCreatePageCommand => _navigateToUserCreatePageCommand ?? (_navigateToUserCreatePageCommand = new DelegateCommand<object>(NavigateToUserCreatePage));
        private async void NavigateToUserCreatePage(object obj)
        {
            await AppService.NavigationService.NavigateAsync(PageNames.UserCreatePage);
        }
    }
}
