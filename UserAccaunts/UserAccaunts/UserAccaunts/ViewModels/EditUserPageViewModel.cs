﻿using System.Windows.Input;
using UserAccaunts.Models;
using UserAccaunts.Services.Remote.Interfaces;
using UserAccaunts.ViewModels.Base;
using Xamarin.Forms;
using Microsoft.Extensions.DependencyInjection;
using UserAccaunts.Dto;
using Prism.Commands;

namespace UserAccaunts.ViewModels
{
    public class EditUserPageViewModel : BaseViewModel
    {
        private readonly IUserService _userService;
        
        public UserModel User { get; set; }

        public EditUserPageViewModel(IAppService appService, IUserService userService)
            : base(appService)
        {
            _userService = userService;
        }

        private DelegateCommand<object> _editUserCommand;
        public DelegateCommand<object> EditUserCommand => _editUserCommand ?? (_editUserCommand = new DelegateCommand<object>(EditUser));
        public async void EditUser(object obj)
        {
            await _userService.Edit(User.Id, new UserDto(User));
        }
    }
}
