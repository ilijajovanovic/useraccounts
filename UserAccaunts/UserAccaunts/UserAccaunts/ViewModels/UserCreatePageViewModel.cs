﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using UserAccaunts.Dto;
using UserAccaunts.Helpers;
using UserAccaunts.Models;
using UserAccaunts.Services;
using UserAccaunts.Services.Remote.Interfaces;
using UserAccaunts.ViewModels.Base;
using UserAccaunts.Views;
using Xamarin.Forms;

namespace UserAccaunts.ViewModels
{
    public class UserCreatePageViewModel : BaseViewModel
    {
        private readonly IUserService _userService;
        public UserModel User { get; set; } = new UserModel();
        public string ButtonText { get; set; } = "Create User";

        public UserCreatePageViewModel(IAppService appService, IUserService userService)
            :base(appService)
        {
            _userService = userService;
        }

        public async void CreateUser()
        {
            //await _userService.CreateUser(User, GenerateRequestUri(Constants.ApiBaseUrl));
        }    
    }
}
