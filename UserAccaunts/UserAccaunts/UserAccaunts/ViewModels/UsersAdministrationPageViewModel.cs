﻿using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using UserAccaunts.Helpers;
using UserAccaunts.Models;
using UserAccaunts.Services;
using UserAccaunts.Services.Remote;
using UserAccaunts.Services.Remote.Interfaces;
using UserAccaunts.ViewModels.Base;
using Xamarin.Forms;

namespace UserAccaunts.ViewModels
{
    public class UsersAdministrationPageViewModel : BaseViewModel
    {
        private readonly IUserService _userService;

        public event PropertyChangedEventHandler PropertyChanged;

        public List<UserModel> Users { get; set; }
        public UserModel User { get; set; }
        public string UserName { get; set; }

        public UsersAdministrationPageViewModel(IAppService appService, IUserService userService)
            :base(appService)
        {
            _userService = userService;
            fetchAllUsers();
        }

        private DelegateCommand<object> _navigateToUserCreatePageCommand;
        public DelegateCommand<object> NavigateToUserCreatePageCommand => 
            _navigateToUserCreatePageCommand ?? (_navigateToUserCreatePageCommand = new DelegateCommand<object>(NavigateToUserCreatePage));
        public async void NavigateToUserCreatePage(object obj)
        {
                    
        }

        private DelegateCommand<object> _deleteUserCommand;
        public DelegateCommand<object> DeleteUserCommand =>
            _deleteUserCommand ?? (_deleteUserCommand = new DelegateCommand<object>(DeleteUser));
        public async void DeleteUser(object obj)
        {
            //await _userService.DeleteUser(GenerateDeleteRequestUri(Constants.ApiBaseUrl, User.Id));
        }

        private DelegateCommand<object> _editUserCommand;
        public DelegateCommand<object> EditUserCommand =>
            _editUserCommand ?? (_editUserCommand = new DelegateCommand<object>(EditUser));
        public async void EditUser(object obj)
        {
            //await Shell.Current.GoToAsync(new ShellNavigationState($"EditUserPage?user={userNavParam}"));
        }

        private DelegateCommand<object> _showSelectedUserCommand;
        public DelegateCommand<object> ShowSelectedUserCommand =>
            _showSelectedUserCommand ?? (_showSelectedUserCommand = new DelegateCommand<object>(ShowSelectedUser));

        public void ShowSelectedUser(object obj)
        {
            UserName = User.FirstName;
        }

        public async void fetchAllUsers()
        {
            var users = await _userService.Get();
            Users = users.Select(x => new UserModel(x)).ToList();
        }
    }
}
