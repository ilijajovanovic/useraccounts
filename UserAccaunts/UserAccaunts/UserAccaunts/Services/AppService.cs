﻿using System;
using System.Collections.Generic;
using System.Text;
using Acr.UserDialogs;
using Prism.Navigation;
using UserAccaunts.Services.Remote.Interfaces;

namespace UserAccaunts.Services
{
    public class AppService : IAppService
    {
        public INavigationService NavigationService { get; }

        public IUserDialogs UserDialogs { get; }

        public AppService(INavigationService navigationService, IUserDialogs userDialogs )
        {
            NavigationService = navigationService;
            UserDialogs = userDialogs;
        }
    }
}
