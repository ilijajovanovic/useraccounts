﻿using Acr.UserDialogs;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;

namespace UserAccaunts.Services.Remote.Interfaces
{
    public interface IAppService
    {
        INavigationService NavigationService { get; }

        IUserDialogs UserDialogs { get; }
    }
}
