﻿using Refit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UserAccaunts.Dto;

namespace UserAccaunts.Services.Remote.Interfaces
{
    public interface IAuthorizationService
    {
        [Post("/users/authenticate")]
        Task<TokenDto> Login([Body]LoginDto dto);

        //[Post("/users/logout")]
        //Task Logout(string username);
    }
}
