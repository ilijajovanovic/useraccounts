﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Refit;
using UserAccaunts.Dto;

namespace UserAccaunts.Services.Remote.Interfaces
{
    public interface IUserService
    {
        [Get("/users")]
        [Headers("Authorization: Bearer")]
        Task<List<UserDto>> Get();

        [Post("/users/authenticate")]
        Task<UserDto> Authenticate([Body]UserDto userDto);

        [Put("/users/{id}")]
        [Headers("Authorization: Bearer")]
        Task<UserDto> Edit(long id, [Body]UserDto userDto);
    }
}
