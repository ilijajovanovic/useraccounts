﻿using Acr.UserDialogs;
using Prism;
using Prism.DryIoc;
using Prism.Ioc;
using UserAccaunts.Helpers;
using UserAccaunts.Services;
using UserAccaunts.Services.Remote.Interfaces;
using UserAccaunts.ViewModels;
using UserAccaunts.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace UserAccaunts
{
    public partial class App : PrismApplication
    {
        public static IContainerProvider ContainerProvider { get; set; }

        public App(IPlatformInitializer initializer = null) : base(initializer) { }

        protected override IContainerExtension CreateContainerExtension() => PrismContainerExtension.Current;

        protected override async void OnInitialized()
        {
            InitializeComponent();
#if DEBUG
            HotReloader.Current.Run(this);
#endif
            ContainerProvider = Container;
            MainPage = new NavigationPage();
            await NavigationService.NavigateAsync(PageNames.LoginPage);
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<LoginPage, LoginPageViewModel>();
            containerRegistry.RegisterForNavigation<EditUserPage, EditUserPageViewModel>();
            containerRegistry.RegisterForNavigation<HomePage, HomePageViewModel>();
            containerRegistry.RegisterForNavigation<UserCreatePage, UserCreatePageViewModel>();
            containerRegistry.RegisterForNavigation<UsersListPage, UserListPageViewModel>();
            containerRegistry.RegisterForNavigation<UsersAdministrationPage, UsersAdministrationPageViewModel>();

            containerRegistry.RegisterInstance(UserDialogs.Instance);
            containerRegistry.RegisterInstance(ServiceFactory<IUserService>.Instance);
            containerRegistry.Register<IAppService, AppService>();


        }        
    }
}
