﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserAccaunts.Helpers
{
    public class PageNames
    {
        public const string LoginPage = nameof(Views.LoginPage);
        public const string HomePage = nameof(Views.HomePage);
        public const string EditUserPage = nameof(Views.EditUserPage);
        public const string UserCreatePage = nameof(Views.UserCreatePage);
        public const string UsersAdministrationPage = nameof(Views.UsersAdministrationPage);
        public const string UsersListPage = nameof(Views.UsersListPage);
    }
}
