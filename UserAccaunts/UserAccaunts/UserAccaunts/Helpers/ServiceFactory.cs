﻿using HttpTracer;
using Newtonsoft.Json;
using Refit;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using UserAccaunts.Services;

namespace UserAccaunts.Helpers
{
    public class ServiceFactory
    {
        protected static HttpClient _client;
        protected static HttpClient Client
        {
            get
            {
                if (_client == null)
                {
                    var tracer = new HttpTracerHandler();
                    //tracer.Verbosity = LogLevel.Information;
                    _client = new HttpClient(new AuthenticatedHttpClientHandler(() => Settings.Token))
                    {
                        BaseAddress = new Uri(Constants.ApiBaseUrl),                       
                    };

                    _client.DefaultRequestHeaders.Add("Accept", "application/json");
                }
                return _client;
            }
        }
    }

    public class ServiceFactory<T> : ServiceFactory
    {
        public static T Instance
        {
            get
            {
                try
                {
                    return RestService.For<T>(Client, new RefitSettings
                    {
                        ContentSerializer = new JsonContentSerializer(new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                        })
                    });
                }
                catch(Exception e)
                {
                    throw;
                }
            }
        }
    }

    //public static class ServiceFactory<T, U> where U : T, new()
    //{
    //    public static T Instance = new U();
    //}
}
