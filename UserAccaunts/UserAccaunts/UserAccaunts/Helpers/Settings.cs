﻿using Newtonsoft.Json;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using UserAccaunts.Dto;
using UserAccaunts.Models;

namespace UserAccaunts.Helpers
{
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants

        private const string SettingsKey = "settings_key";
        private static readonly string SettingsDefault = string.Empty;
        private const string TokenKey = "Token";
        private const string LoggedInUserKey = "LoggedInUser";

        #endregion
        public static string Token
        {
            get => JsonConvert.DeserializeObject<string>(AppSettings.GetValueOrDefault(TokenKey, string.Empty));
            set => AppSettings.AddOrUpdateValue(TokenKey, JsonConvert.SerializeObject(value));
        }

        public static UserModel LoggedInUser
        {
            get => JsonConvert.DeserializeObject<UserModel>(AppSettings.GetValueOrDefault(LoggedInUserKey, string.Empty));
            set => AppSettings.AddOrUpdateValue(LoggedInUserKey, JsonConvert.SerializeObject(value));
        }

        public static string GeneralSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(SettingsKey, SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(SettingsKey, value);
            }
        }

    }
}

