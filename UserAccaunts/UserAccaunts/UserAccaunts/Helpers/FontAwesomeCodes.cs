﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserAccaunts.Helpers
{
    public static class FontAwesomeCodes
    {
        public const string Canabis = "\uf55f";
        public const string Home = "\uf015";
        public const string SignIn = "\uf090";
        public const string Users = "\uf0c0";
        public const string UserPlus = "\uf234";
        public const string BattleNet = "\uf835";
        public const string List = "\uf03a";
        public const string User = "\uf007";
        public const string Key = "\uf084";
    }
}
