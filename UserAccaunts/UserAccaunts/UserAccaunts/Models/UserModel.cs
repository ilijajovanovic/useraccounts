﻿using System;
using System.Collections.Generic;
using System.Text;
using UserAccaunts.Dto;

namespace UserAccaunts.Models
{
    public class UserModel
    {
        public long Id { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Adress { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }

        public UserModel(UserDto dto)
        {
            Id = dto.Id;
            Username = dto.Username;
            FirstName = dto.FirstName;
            LastName = dto.LastName;
            Email = dto.Email;
            Adress = dto.Adress;
            PhoneNumber = dto.PhoneNumber;
            Password = dto.Password;
            Token = dto.Token;
        }
        public UserModel()
        {
        }
    }
}
