﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserAccaunts.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UserAccaunts
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AppShell : Shell
    {
        public AppShell()
        {
            InitializeComponent();

            Routing.RegisterRoute("LoginPage", typeof(LoginPage));

            Routing.RegisterRoute("HomePage", typeof(HomePage));
            Routing.RegisterRoute("EditUserPage", typeof(EditUserPage));
            Routing.RegisterRoute("UserCreatePage", typeof(UserCreatePage));
            Routing.RegisterRoute("UsersListPage", typeof(UsersListPage));

            
        }
    }
}